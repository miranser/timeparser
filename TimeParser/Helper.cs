﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace TimeParser
{
    public partial class MainWindow
    {
        public void ResizeImage(string path, string oPath, string swidth, string sheight)
        {
            int width = Convert.ToInt32(swidth);
            int height = Convert.ToInt32(sheight);
            var newPath = Path.Combine(oPath, "resized");

            var newFileName = FileTimeString(new FileInfo(path).LastWriteTime) + extension;
            Directory.CreateDirectory(newPath);
            Image image = Image.FromFile(path);
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);

                }
            }

            if (extension == ".JPG")
                destImage.Save(Path.Combine(newPath, newFileName), ImageFormat.Jpeg);
            if (extension == ".PNG")
                destImage.Save(Path.Combine(newPath, newFileName), ImageFormat.Png);
            if (extension == ".BMP")
                destImage.Save(Path.Combine(newPath, newFileName), ImageFormat.Bmp);
            if (extension == ".GIF")
                destImage.Save(Path.Combine(newPath, newFileName), ImageFormat.Gif);
        }

        private void ProcessDirectory(string path, string oPath)
        {
            Directory.CreateDirectory(oPath);
            var counter = 0;
            string hmline = "";
            string line = "";
            string fileName = "";
            var fileEntries = Directory.GetFiles(path).Where(file => Path.GetExtension(file) != ".db");
            Bar.Maximum = fileEntries.Count();
            var timer = new DispatcherTimer();
            foreach (var file in fileEntries)
            {
                Thread t = new Thread(
                    new ThreadStart(
                        delegate
                        {
                            var dispOp =
                                Bar.Dispatcher.BeginInvoke(DispatcherPriority.Loaded,
                                    new Action(
                                        delegate
                                        {
                                            var oFileInfo = new FileInfo(file);
                                            GetFileName(ref fileName, file, oFileInfo);
                                            if (copyFilesCheckBox.IsChecked == true)
                                            {
                                                CopyFile(file, oPath, fileName);
                                            }
                                            if (resizeCheckBox.IsChecked == true)
                                            {
                                                ResizeImage(file, oPath, widthTextBox.Text, heightTextBox.Text);
                                            }
                                            if (fulltimeCheckBox.IsChecked == true)
                                            {
                                                WriteFulltimeTxt(oPath, fileName, oFileInfo);
                                            }
                                            if (hmCheckBox.IsChecked == true)
                                            {
                                                WriteHoursMinutesTxt(oPath, oFileInfo, counter);
                                            }
                                            Bar.Value = ++counter;
                                            Thread.Sleep(1);
                                        }
                                        ));
                        }
                        ));
                t.Start();
            }
        }

        private void WriteFulltimeTxt(string path, string fileName, FileInfo oFileInfo)
        {
            string line = fileName + "," + oFileInfo.LastWriteTime;
            File.AppendAllText(Path.Combine(path, "Fulltime.txt"), line + "\r\n");
        }
        private void WriteHoursMinutesTxt(string path, FileInfo oFileInfo, int counter)
        {
            string line = oFileInfo.LastWriteTime.ToString("t", new CultureInfo("hr-HR"));
            if (counter % 2 != 0)
                File.AppendAllText(Path.Combine(path, "ShortTime.txt"), line + "\r\n");
        }
        private string FileTimeString(DateTime FullDate)
        {
            var tmpArr = FullDate.ToString("G", new CultureInfo("de-DE")).Split(' ');
            var timeArr = tmpArr[1].Split(':');
            return tmpArr[0] + "_" + timeArr[0] + "-" + timeArr[1] + "-" + timeArr[2];
        }
        private void GetFileName(ref string fileName, string file, FileInfo oFileInfo)
        {
            fileName = Path.GetFileName(file);
            if (renameCheckBox.IsChecked == true)
            {
                fileName = FileTimeString(oFileInfo.LastWriteTime) + Path.GetExtension(file);
            }
        }
        private void CopyFile(string file, string oPath, string fileName)
        {
            if (copyFilesCheckBox.IsChecked == true)
            {
                if (!File.Exists(Path.Combine(oPath, fileName)))
                {
                   
                    File.Copy(file, Path.Combine(oPath, fileName));
                    File.SetAttributes(Path.Combine(oPath, fileName), FileAttributes.Normal);
                }
            }
        }
        
    }
}
