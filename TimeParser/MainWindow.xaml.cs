﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using MahApps.Metro;
using MahApps.Metro.Controls;
using Application = System.Windows.Application;
using ComboBox = System.Windows.Controls.ComboBox;
using DataFormats = System.Windows.DataFormats;
using DragDropEffects = System.Windows.DragDropEffects;
using DragEventArgs = System.Windows.DragEventArgs;
using MessageBox = System.Windows.Forms.MessageBox;
using TextBox = System.Windows.Controls.TextBox;

namespace TimeParser
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private string extension = ".PNG";

        public MainWindow()
        {
            InitializeComponent();
            widthTextBox.Text = "209";
        }


        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            var folderBD = new FolderBrowserDialog();
            folderBD.SelectedPath = @"C:\";
            if (folderBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                folderTextBox.Text = folderBD.SelectedPath;
                outTextBox.Text = folderBD.SelectedPath;
            }
        }

        private void outFolderButton_Click(object sender, RoutedEventArgs e)
        {
            var folderBD = new FolderBrowserDialog();

            folderBD.SelectedPath = @folderTextBox.Text;
            if (folderBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                outTextBox.Text = folderBD.SelectedPath;
            }
        }

        private void processButton_Click(object sender, RoutedEventArgs e)
        {
            string[] answers =
            {
                "Processed. w/out errors. may be. i'm not sure:)"
                , "Processed"
                , "Yeap."
                , "Don't look at the result. i'm not sure it works"
                , "Really?"
                , "This message? it works? i can't believe."
                , "All right"
                , "Main part of your work has beed made"
                ,
                "OH NOOOOOOOOOOOOO! IT'S A CRITICAL FAILURE! OH MY GOOOOOOODD NOOOO!!! \n joke. all right:)\n may be..."
            };
            var rnd = new Random();
            if (folderTextBox.Text != "")
            {
                ProcessDirectory(folderTextBox.Text, outTextBox.Text);
                MessageBox.Show(answers[rnd.Next(0, answers.Length)]);
            }
            else
            {
                MessageBox.Show("НЕ ПЫТАЙСЯ МЕНЯ НАЕБАТЬ!");
            }
        }

        private void themeButton_Click(object sender, RoutedEventArgs e)
        {
            var appStyle = ThemeManager.DetectAppStyle(Application.Current);

            string[] themes =
            {
                "Red", "Green", "Blue", "Purple", "Orange", "Lime", "Emerald", "Teal", "Cyan", "Cobalt",
                "Indigo", "Violet", "Pink", "Magenta", "Crimson", "Amber", "Yellow", "Brown", "Olive", "Steel", "Mauve",
                "Taupe", "Sienna"
            };
            string[] accents = {"BaseDark", "BaseLight"};
            var tRnd = new Random();
            var aRnd = new Random();
            // now set the Green accent and dark theme
            ThemeManager.ChangeAppStyle(Application.Current,
                ThemeManager.GetAccent(themes[tRnd.Next(0, themes.Length)]),
                ThemeManager.GetAppTheme(accents[aRnd.Next(0, 2)]));
        }

        private void widthTextBox_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            heightTextBox.Text = (Convert.ToInt32(widthTextBox.Text)*16/9).ToString();
        }

        private void heightTextBox_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            widthTextBox.Text = (Convert.ToInt32(heightTextBox.Text)*9/16).ToString();
        }

        private static bool IsTextAllowed(string text)
        {
            var regex = new Regex("[^0-9]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof (string)))
            {
                var text = (string) e.DataObject.GetData(typeof (string));
                if (!IsTextAllowed(text))
                {
                    e.CancelCommand();
                }
            }
            else
            {
                e.CancelCommand();
            }
        }

        private void widthTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (widthTextBox.Text != "")
                heightTextBox.Text = (Convert.ToInt32(widthTextBox.Text)*16/9).ToString();
        }

        private void heightTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //widthTextBox.Text = (Convert.ToInt32(heightTextBox.Text) * 9 / 16).ToString();
        }

        private void heightTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (widthTextBox.Text != "")
                heightTextBox.Text = (Convert.ToInt32(widthTextBox.Text)*16/9).ToString();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = (ComboBox) sender;
            var selectedItem = (ComboBoxItem) comboBox.SelectedItem;
            extension = selectedItem.Content.ToString();
        }

        private void sometb_PreviewDragEnter(object sender, DragEventArgs e)
        {
            e.Handled = true;
            e.Effects = DragDropEffects.Copy;
        }

        private void sometb_PreviewDrop(object sender, DragEventArgs e)
        {
            var text = e.Data.GetData(DataFormats.FileDrop);
            var tb = sender as TextBox;
            if (tb != null)
            {
                tb.Text = string.Format("{0}", ((string[]) text)[0]);
            }
        }

        private void resizeCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            widthTextBox.IsEnabled = true;
            heightTextBox.IsEnabled = true;
            extensionComboBox.IsEnabled = true;
        }

        private void copyFilesCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            renameCheckBox.IsEnabled = true;
        }

        private void copyFilesCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            renameCheckBox.IsEnabled = false;
            renameCheckBox.IsChecked = false;
        }

        private void resizeCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            widthTextBox.IsEnabled = false;
            heightTextBox.IsEnabled = false;
            extensionComboBox.IsEnabled = false;
        }
    }
}